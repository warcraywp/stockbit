package com.stockbit.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.stockbit.common.base.BaseHolder
import com.stockbit.common.base.BasePagingAdapter
import com.stockbit.model.SponsoredDataItem
import com.stockbit.ui.databinding.ItemCryptoBinding

class CryptoPagingAdapter :
    BasePagingAdapter<SponsoredDataItem, CryptoPagingAdapter.ViewHolder>(
        DIFF_CALLBACK
    ) {


    inner class ViewHolder(view: ItemCryptoBinding) :
        BaseHolder<SponsoredDataItem>(view.root) {
        private val bind = view
        fun bind() = with(itemView) {
            itemData?.let { data ->
                bind.tvName.text = data.coinInfo?.name
                bind.tvFullname.text = data.coinInfo?.fullname
                bind.tvPrice.text = data.rAW?.uSD?.price
                bind.tvChanges.text =data.rAW?.uSD?.changes
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding =
            ItemCryptoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(
            itemBinding
        )
    }


    override fun bindViewHolder(holder: ViewHolder?, data: SponsoredDataItem?, position: Int) {
        holder?.bind()
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<SponsoredDataItem>() {
            override fun areItemsTheSame(oldItem: SponsoredDataItem, newItem: SponsoredDataItem): Boolean {
                return oldItem.coinInfo?.name == newItem.coinInfo?.name

            }

            override fun areContentsTheSame(oldItem: SponsoredDataItem, newItem: SponsoredDataItem): Boolean {
                return oldItem == newItem
            }
        }
    }

}