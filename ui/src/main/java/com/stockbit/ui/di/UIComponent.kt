package com.stockbit.ui.di

import com.stockbit.ui.home.HomeViewModel
import com.stockbit.ui.login.LoginViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

@InternalCoroutinesApi
val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { LoginViewModel(get()) }
}