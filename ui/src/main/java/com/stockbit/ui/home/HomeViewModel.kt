package com.stockbit.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import com.stockbit.common.base.BaseViewModel
import com.stockbit.model.SponsoredDataItem
import com.stockbit.repository.ExampleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@InternalCoroutinesApi
class HomeViewModel(val repository: ExampleRepository) : BaseViewModel() {
    private var homeJob: Job? = null
    private var _crypto: MutableLiveData<PagingData<SponsoredDataItem>> = MutableLiveData()
    var crypto: LiveData<PagingData<SponsoredDataItem>> = _crypto
    fun getData() {
        homeJob?.cancel()
        viewModelScope.launch(Dispatchers.IO) {
            getCrypto()
        }
    }

    private suspend fun getCrypto() {
        repository.getExample().collect {
            _crypto.postValue(it)
        }
    }

}