package com.stockbit.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.stockbit.common.base.BaseFragment
import com.stockbit.common.base.BaseViewModel
import com.stockbit.ui.adapter.CryptoPagingAdapter
import com.stockbit.ui.databinding.HomeFragmentBinding
import com.stockbit.ui.home.HomeViewModel
import kotlinx.coroutines.InternalCoroutinesApi
import org.koin.android.viewmodel.ext.android.viewModel

@InternalCoroutinesApi
class HomeFragment : BaseFragment<HomeFragmentBinding>() {

    private val homeViewModel: HomeViewModel by viewModel()
    private val mainAdapter = CryptoPagingAdapter()

    override fun getViewModel(): BaseViewModel {
        return homeViewModel
    }

    override fun getBind(i: LayoutInflater, v: ViewGroup?): HomeFragmentBinding {
        return HomeFragmentBinding.inflate(i, v, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeView()
    }

    private fun initView() {
        bind.swipeLayout.setOnRefreshListener {
            mainAdapter.refresh()
        }
        bind.rvHome.run {
            adapter = mainAdapter
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }
        mainAdapter.addLoadStateListener { loadStates ->
            bind.swipeLayout.isRefreshing = loadStates.refresh is LoadState.Loading
        }
        homeViewModel.getData()
    }

    private fun observeView() {
        homeViewModel.crypto.observe(viewLifecycleOwner, {
            mainAdapter.submitData(lifecycle, it)
        })
    }


}