package com.stockbit.ui.login

import com.stockbit.common.base.BaseViewModel
import com.stockbit.repository.ExampleRepository

class LoginViewModel(val repository: ExampleRepository) : BaseViewModel() {
    private val correctCredential = "user"

    fun getCorrectUsernameAndPassword(userName: String, password: String): Boolean {
        return userName == correctCredential && password == correctCredential
    }
}