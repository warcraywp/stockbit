package com.stockbit.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stockbit.common.base.BaseFragment
import com.stockbit.common.base.BaseViewModel
import com.stockbit.ui.databinding.LoginFragmentBinding
import org.koin.android.viewmodel.ext.android.viewModel

class LoginFragment : BaseFragment<LoginFragmentBinding>() {
    private val loginViewModel: LoginViewModel by viewModel()

    override fun getViewModel(): BaseViewModel {
        return loginViewModel
    }

    override fun getBind(i: LayoutInflater, v: ViewGroup?): LoginFragmentBinding {
        return LoginFragmentBinding.inflate(i, v, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeView()
    }

    private fun observeView() {
        bind.tvLogin.setOnClickListener {
            if (loginViewModel.getCorrectUsernameAndPassword(
                    bind.etUsername.text.toString(),
                    bind.etPassword.text.toString()
                )
            ) {
                val navigation = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                loginViewModel.navigate(navigation)
            }
        }
    }
}