package com.stockbit.common.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseHolder<T>(
    itemView: View
) :
    RecyclerView.ViewHolder(itemView) {
    private var itemPosition = 0
    var itemData: T? = null
    private var itemSelected = 0


    fun bindData(position: Int, data: T?) {
        itemPosition = position
        itemData = data
    }

}