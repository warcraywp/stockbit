package com.stockbit.repository

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.stockbit.model.SponsoredDataItem
import com.stockbit.remote.ExampleDatasource

class DataPagingSource(
    private val service: ExampleDatasource,
) : PagingSource<Int, SponsoredDataItem>() {
    private val startingIndex = 1
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, SponsoredDataItem> {
        val position = params.key ?: startingIndex
        val response = service.fetchCryptoData(position)
        val nextKey = if (!response.isSuccessful) {
            null
        } else {
            position + 1
        }
        return LoadResult.Page(
            data = response.body()?.data.orEmpty(),
            prevKey = if (position == startingIndex) null else position - 1,
            nextKey = nextKey
        )
    }

    override fun getRefreshKey(state: PagingState<Int, SponsoredDataItem>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

}