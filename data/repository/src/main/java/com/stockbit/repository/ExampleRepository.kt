package com.stockbit.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.stockbit.local.dao.ExampleDao
import com.stockbit.model.SponsoredDataItem
import com.stockbit.remote.ExampleDatasource
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow

interface ExampleRepository {
    suspend fun getExample(): Flow<PagingData<SponsoredDataItem>>
}

class ExampleRepositoryImpl(
    private val datasource: ExampleDatasource,
    private val dao: ExampleDao
) : ExampleRepository {

    @InternalCoroutinesApi
    override suspend fun getExample(): Flow<PagingData<SponsoredDataItem>> {
        return flow {
            getMoviePager(datasource).collect {
                val data = it
                emit(data)
            }
        }
    }


    private fun getMoviePager(service: ExampleDatasource) =
        Pager(
            config = PagingConfig(pageSize = 10, enablePlaceholders = false),
            pagingSourceFactory = { DataPagingSource(service = service) }
        ).flow

}