package com.stockbit.remote

import com.stockbit.model.FullDataModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ExampleService {

    @GET("data/top/totaltoptiervolfull?limit=10&tsym=USD")
    suspend fun fetchExampleAsync(@Query("page") page: Int): Response<FullDataModel>

}