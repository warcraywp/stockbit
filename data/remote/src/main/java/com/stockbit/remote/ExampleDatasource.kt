package com.stockbit.remote

/**
 * Implementation of [ExampleService] interface
 */
class ExampleDatasource(private val exampleService: ExampleService) {

    suspend fun fetchCryptoData(page: Int) =
        exampleService.fetchExampleAsync(page)

}