package com.stockbit.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class FullDataModel(

    @PrimaryKey
    @SerializedName("Message")
    val message: String,

    @SerializedName("Data")
    val data: List<SponsoredDataItem>
)

data class SponsoredDataItem(
    @field:SerializedName("CoinInfo")
    val coinInfo: ExampleModel? = null,
    @field:SerializedName("RAW")
    val rAW: RAW? = null
)

data class RAW(

    @SerializedName("USD")
    val uSD: USD? = null
)

data class USD(
    @SerializedName("PRICE")
    val price: String,
    @SerializedName("CHANGE24HOUR")
    val changes: String
)

