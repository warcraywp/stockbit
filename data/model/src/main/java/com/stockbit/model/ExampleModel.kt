package com.stockbit.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class ExampleModel(

    @PrimaryKey
    @SerializedName("Id")
    val id: String,

    @SerializedName("Name")
    val name: String,

    @SerializedName("FullName")
    val fullname: String,

    @SerializedName("ImageUrl")
    val image: String,


)

